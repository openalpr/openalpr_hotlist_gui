from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('widgets/kv/checkbox.kv')


class LabeledCheckbox(BoxLayout):

    group = ObjectProperty(None, allownone=True)
    allow_no_selection = BooleanProperty()
    text = StringProperty()
    active = BooleanProperty(False)
    font_size = NumericProperty(20)

    def __init__(self, **kwargs):
        super(LabeledCheckbox, self).__init__(**kwargs)
        self.register_event_type('on_changed')

    def on_chk_changed(self):
        self.active = self.ids.chk.active
        self.dispatch('on_changed')

    def on_touch_down(self, touch):
        if self.ids.label.collide_point(touch.x, touch.y):
            self.ids.chk._do_press()
        super(LabeledCheckbox, self).on_touch_down(touch)

    def on_changed(self):
        pass
