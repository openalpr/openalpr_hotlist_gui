from kivy.animation import Animation
from kivy.properties import BooleanProperty, StringProperty

from kivymd.textfields import MDTextField
from settings import US_STATES
from widgets.base import DefaultInput


class ALPRMDTextField(MDTextField, DefaultInput):

    write_tab = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.helper_text_mode = 'on_error'

    def mark_as_normal(self):
        self.error = False

    def mark_as_error(self):
        self.error = True
        Animation(duration=.2, _current_error_color=self.error_color).start(self)

    def on_touch_down(self, touch):
        super(ALPRMDTextField, self).on_touch_down(touch)
        self.error = False

    def _get_value(self):
        return self.text

    def custom_error_check(self):
        pass

    def set_value(self, value):
        self.text = value


class StateImportInput(ALPRMDTextField):

    helper_text = StringProperty("Invalid State Name(s)")

    def custom_error_check(self):
        if self.text and any([(s.upper() not in US_STATES and s.capitalize() not in US_STATES.values())
                              for s in self.text.split(',')]):
            return [True]
