from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, OptionProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout

from widgets.base import DefaultInput


Builder.load_file('widgets/kv/parser_item.kv')


class ParserItem(BoxLayout, DefaultInput):

    name = StringProperty()
    code = StringProperty()
    match_strategy = OptionProperty('Exact', options=['Exact', 'Lenient'])
    hotlist_path = StringProperty()
    is_last = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._init_values = kwargs
        self.register_event_type('on_remove')
        self.register_event_type('on_add')
        self.register_event_type('on_changed')

    def on_content_changed(self, wid, key):
        if wid.text != self._init_values.get(key, ""):
            self.dispatch('on_changed')
        if key == 'hotlist_path':
            self.ids.chk_override.active = len(wid.text) > 0

    def mark_as_normal(self):
        for widget in self.widgets():
            widget.mark_as_normal()

    def mark_as_error(self):
        for widget in self.widgets():
            widget.mark_as_error()

    def set_value(self, value):
        for widget in self.widgets():
            if hasattr(widget, 'key'):
                val = value.get(widget.key)
                if val:
                    widget.set_value(val)

    def _get_value(self):
        data = {}
        for widget in self.widgets():
            if not widget.is_empty():
                data.update(widget.get_value())
        return data

    def custom_error_check(self):
        pass

    def widgets(self):
        for widget in self.walk(restrict=True):
            if isinstance(widget, DefaultInput) and hasattr(widget, 'key'):
                if widget.key:
                    yield widget

    def validate(self):
        v = {}
        for w in self.widgets():
            v.update(w.validate())
        return v

    def on_chk_override_changed(self):
        if not self.ids.chk_override.active:
            self.ids.txt_hotlist_path.text = ''
            self.ids.txt_hotlist_path.mark_as_normal()
        self.ids.txt_hotlist_path.required = self.ids.chk_override.active
        self.ids.txt_hotlist_path.on_focus()
        self.dispatch('on_changed')

    def on_changed(self):
        pass

    def on_remove(self):
        pass

    def on_add(self):
        pass

    def on_btn_minus(self):
        self.dispatch('on_remove')

    def on_btn_plus(self):
        self.dispatch('on_add')

    def is_override(self):
        return self.ids.chk_override.active

    def get_hotlist_path(self):
        return self.ids.txt_hotlist_path.text

    def on_info_btn(self, btn, content=''):
        app = App.get_running_app()
        x, y = app.current_screen.to_window(*self.to_window(*btn.pos))
        app.show_hint_popup(x, y, content)
