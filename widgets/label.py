from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, NumericProperty
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView

from widgets.icons import ICONS


Builder.load_file('widgets/kv/label.kv')


class ScrollableLabel(ScrollView):
    text = StringProperty('')
    color = ListProperty([0, 0, 0, 1])
    font_size = NumericProperty(18)


class ColoredLabel(Label):
    background_color = ListProperty([1, 1, 1, 0])
    color = ListProperty([0, 0, 0, 1])


class IconLabel(ColoredLabel):
    icon = StringProperty("")
    font_name = StringProperty("Icon")

    def on_icon(self, *args):
        self.text = ICONS.get(self.icon, "")
