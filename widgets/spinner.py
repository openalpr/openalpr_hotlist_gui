from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.uix.spinner import Spinner
from kivy.clock import Clock
from kivymd.button import MDRaisedButton
from widgets.button import ALPRButton


Builder.load_file('widgets/kv/spinner.kv')


class ALPRSpinnerOption(ALPRButton):
    size_hint_y = NumericProperty(None)


class ALPRSpinner(MDRaisedButton, Spinner):
    option_cls = ObjectProperty(ALPRSpinnerOption)
    capitalized = False
    font_style = 'Subhead'
    value = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.register_event_type('on_changed')

    def on_text(self, instance, value):
        super().on_text(instance, value)
        self.value = self.text

    def _update_dropdown(self, *largs):
        super()._update_dropdown(*largs)
        Clock.schedule_once(lambda dt: self._update_width())

    def _update_width(self):
        for wid in self._dropdown.walk(restrict=True):
            if isinstance(wid, self.option_cls):
                wid.width = self.width

    def _on_dropdown_select(self, *args):
        super()._on_dropdown_select(*args)
        self.dispatch('on_changed')

    def on_values(self, *args):
        if self.values:
            self.text = self.values[0]
        self.dispatch('on_changed')

    def get_value(self):
        return self.value

    def set_value(self, val):
        self.text = val

    def on_changed(self):
        pass
