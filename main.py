import gc
import traceback

from kivy.app import App
import config_kivy

from kivy.uix.screenmanager import SlideTransition, NoTransition
from kivy.base import ExceptionHandler, ExceptionManager
from kivy.input.providers.mouse import MouseMotionEvent
from kivymd.theming import ThemeManager
from screens.screen_manager import sm, screens
from settings import INIT_SCREEN, WIDTH, HEIGHT
from utils.common import logger
from widgets.dialog import InfoDialog

import widgets.factory_reg

# Needed to remove red circles from right/mid mouse buttons clicks
MouseMotionEvent.update_graphics = lambda *args: None


class CustomExceptionHandler(ExceptionHandler):
    """
    Exception Handler class to catch any exception and to show the error screen
    """

    def handle_exception(self, exception):
        """
        Catch the exception and show the error screen
        :param exception:
        :return:
        """
        logger.exception(exception)
        cur_app = App.get_running_app()
        # Save the traceback of the current exception.
        cur_app.save_exception(traceback.format_exc(limit=20))
        cur_app.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(CustomExceptionHandler())


class OpenALPRHotlistImporterApp(App):

    current_screen = None
    exception = None
    theme_cls = ThemeManager()

    def build(self):
        self.title = 'OpenALPR Hotlist Importer'
        self.switch_screen(INIT_SCREEN)
        return sm

    def switch_screen(self, screen_name, direction=None, duration=.3):
        """
        Switch to the other screen
        :param screen_name: destination screen name
        :param direction: direction of transition
        :param duration: transition time
        :return:
        """
        if sm.has_screen(screen_name):                  # Already in the destination screen?
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction, duration=duration)
            else:
                sm.transition = NoTransition()

            sm.switch_to(screen)

            # logger.info('=== Switched to {} screen'.format(screen_name))

            # Delete the old screen

            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def save_exception(self, ex):
        """
        This function is called by the Exception Handler.
        :param ex:
        :return:
        """
        self.exception = ex

    def get_exception(self):
        return self.exception

    @staticmethod
    def show_hint_popup(x, y, content):
        popup = InfoDialog()
        popup.text = content
        x = x if (x < WIDTH - len(content) * 7) else (x - len(content) * 7)
        y = (y - 15) if y > 20 else (y + 15)
        popup.pos_hint = {'center_x': (x + len(content) * 4) / WIDTH, 'center_y': y / HEIGHT}
        popup.open()


if __name__ == '__main__':

    logger.info('========== Staring openALPR Hotlist Importer App ==========')
    _app = OpenALPRHotlistImporterApp()
    _app.run()
