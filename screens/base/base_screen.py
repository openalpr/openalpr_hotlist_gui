import threading

from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen

from settings import WIDTH, HEIGHT
from utils.common import logger
from widgets.base import DefaultInput
from widgets.dialog import InfoDialog


class BaseScreen(Screen):

    app = ObjectProperty()

    def __init__(self, **kwargs):
        self.app = App.get_running_app()
        self._b_stop = threading.Event()
        self._b_stop.clear()
        super().__init__(**kwargs)

    def switch_screen(self, screen_name, direction=None):
        self.app.switch_screen(screen_name, direction)

    def on_pre_leave(self, *args):
        super(BaseScreen, self).on_pre_leave(*args)
        self._b_stop.set()

    def widgets(self, container=None):
        if container is None:
            container = self
        for widget in container.walk(restrict=True):
            if isinstance(widget, DefaultInput) and hasattr(widget, 'key'):
                if widget.key:
                    yield widget

    def collect_data(self, container=None):
        """
        Walk through child widgets and collect data from inputs
        :return: dict
        """
        data = {}
        for widget in self.widgets(container):
            if not widget.is_empty():
                data.update(widget.get_value())
        return data

    def set_data(self, data, container=None):
        """
        Walk through child widgets and set inputs
        :return: dict
        """

        for widget in self.widgets(container):
            if hasattr(widget, 'key'):
                val = data.get(widget.key)
                if val:
                    # Because, unlike normal widgets, spinner's set_value sets it's options, not value
                    # for spinner set_current_value is needed
                    method = 'set_value'
                    if hasattr(widget, 'set_current_value'):
                        method = 'set_current_value'
                    getattr(widget, method)(val)

    def _validate_fields(self, container=None):
        """
        Walk through screen input widgets (inherits from widgets.default.DefaultInput)
        and collect errors
        Returns a dict of field key and errors list similar to the server response
        :return: dict
        """
        result = dict()
        for widget in self.widgets(container):
            result.update(widget.validate())

        return result

    def show_errors(self, data, container=None):
        """
        Display response errors on the screen input widgets
        :param container:
        :param data: dict
        :return:
        """
        for widget in self.widgets(container):
            error = data.get(widget.key)
            if error:
                if hasattr(widget, 'show_error'):
                    if isinstance(data[widget.key], list):
                        widget.show_error(data[widget.key][0])
                    else:
                        widget.show_error(str(data[widget.key]))

    def clear_errors(self, container=None):
        for widget in self.widgets(container):
            widget.clear_errors()

    def is_valid(self, container=None):
        """
        Collect fields errors and show if any.
        :return: bool
        """
        self.clear_errors(container)
        errors = self._validate_fields(container)
        if errors:
            self.show_errors(errors, container)
            logger.warning('Validation: {}'.format(errors))
            return False

        return True

    def on_btn_info(self, widget, content=""):
        widget_x, widget_y = self.to_window(widget.x, widget.y)
        self.app.show_hint_popup(widget_x, widget_y, content)
