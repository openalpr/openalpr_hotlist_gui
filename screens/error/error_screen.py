from kivy.lang import Builder

from screens.base.base_screen import BaseScreen
from settings import INIT_SCREEN


Builder.load_file('screens/error/error_screen.kv')


class ErrorScreen(BaseScreen):

    def on_enter(self, *args):
        error_msg = self.app.get_exception()
        self.ids.error_msg.text = str(error_msg)
        self.ids.error_msg.scroll_y = 0
        super(ErrorScreen, self).on_enter(*args)

    def on_btn_back(self):
        self.switch_screen(INIT_SCREEN)
