"""
    Pre-configuration file to be used to setup Kivy configurations.
    This module must be called before executing a Kivy GUI app!
"""
import os
from kivy.clock import Clock
from kivy.config import Config
from settings import WIDTH, HEIGHT
from kivy.core.text import LabelBase


Config.read(os.path.expanduser('~/.kivy/config.ini'))

Config.set('graphics', 'width', str(WIDTH))
Config.set('graphics', 'height', str(HEIGHT))
Config.set('graphics', 'resizable', False)
Config.set('kivy', 'keyboard_mode', 'system')
Config.set('kivy', 'keyboard_layout', 'en_US')
Config.set('kivy', 'log_level', 'info')


Clock.max_iteration = 20

LabelBase.register(name="Icon", fn_regular="assets/fonts/materialdesignicons-webfont.ttf")
